CFLAGS = -O2 -Wall -Wstrict-prototypes

VERSION=1.0
DATE="July 6, 2006"

prefix ?= /usr
bindir ?= $(prefix)/bin
sbindir ?= $(prefix)/sbin
mandir ?= $(prefix)/share/man/man8
miscdir ?= $(prefix)/share/misc

all:	lspnp lspnp.8 setpnp setpnp.8

clean:
	rm -f *.o *.8 lspnp setpnp

%.8: %.man
	M=`echo $(DATE)`; sed <$< >$@ "s/@TODAY@/$$M/;s/@VERSION@/pnputils-$(VERSION)/;s|@MISCDIR@|$(miscdir)|g;"

install: all
	install -m 755 -d $(DESTDIR)$(bindir)
	install -m 755 lspnp $(DESTDIR)$(bindir)
	install -m 755 -d $(DESTDIR)$(sbindir)
	install -m 755 setpnp $(DESTDIR)$(sbindir)
	install -m 755 -d $(DESTDIR)$(mandir)
	install -m 644 lspnp.8 setpnp.8 $(DESTDIR)$(mandir)
	install -m 755 -d $(DESTDIR)$(miscdir)
	install -m 644 pnp.ids $(DESTDIR)$(miscdir)

REL=pnputils-$(VERSION)
DISTTMP=/tmp/pnputils-dist

dist: clean
	git archive --format=tar --prefix=pnputils-$(VERSION)/ HEAD \
		| gzip -9 > ../pnputils-$(VERSION).tar.gz

.PHONY: all clean install dist

lspnp.o: CPPFLAGS+=-DMISCDIR="\"$(miscdir)\""
lspnp.o: lspnp.c pnp_resource.h
setpnp.o: setpnp.c pnp_resource.h
