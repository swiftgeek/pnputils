#!/bin/sh
# retrieve PNP ids from freebsd source code
wget -c -O acpidevs http://cvsweb.netbsd.org/bsdweb.cgi/~checkout~/src/sys/dev/acpi/acpidevs
more +15 acpidevs > out
sed -i -e 's/\t/ /g' out
sed -i -e 's/# /#/g' out
