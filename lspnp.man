.\" Copyright (C) 1999 David A. Hinds -- dahinds@users.sourceforge.net
.\" (c) Copyright 2006 Hewlett-Packard Development Company, L.P.
.\"	Bjorn Helgaas <bjorn.helgaas@hp.com>
.\"
.TH lspnp 8 "@TODAY@" "@VERSION@" "Linux Plug and Play Utilities"
.SH NAME
lspnp \- list Plug and Play BIOS device nodes and resources
.SH SYNOPSIS
.B lspnp
.RB [ -b ]
.RB [ -v [ v ]]
.RI [ "device ..." ]
.SH DESCRIPTION
This utility presents a formatted interpretation of the contents of the
.I /sys/bus/pnp/devices
or
.I /proc/bus/pnp
tree.  The default output is a list of Plug and Play device node
names, product identifiers, and descriptions.  Verbose output
.RB ( -v )
includes resource allocations (IO ports, memory, interrupts, and DMA 
channels) for each device.  Very verbose output
.RB ( -vv )
includes lists of possible resources, various configuration flags, and
product identifiers for compatible devices.
.PP
The output can be limited to one or more specific device nodes by
specifying their node names on the command line.  By
default, current (dynamic) device configuration information is
displayed; with the
.B -b
option, the boot (static) configuration is shown.
.SH OPTIONS
.TP
.B \-b
Boot mode: read device resource information that will be used at next
boot (as opposed to current resource info).  This is supported only on
kernels that provide the old
.I /proc/bus/pnp
interface.
.TP
.B \-v
Selects more verbose output.  Can be used more than once.
.SH FILES
.TP \w'@MISCDIR@/pnp.ids\ \ \ \ |\|'u
@MISCDIR@/pnp.ids
A database of known Plug and Play device ID's.
.TP
/sys/bus/pnp/devices/...
The kernel interface for Plug and Play BIOS device services.
.TP
/proc/bus/pnp/...
The old kernel interface for Plug and Play BIOS device services.
.SH AUTHORS
David Hinds \- dahinds@users.sourceforge.net
.br
Bjorn Helgaas \- bjorn.helgaas@hp.com
.SH "SEE ALSO"
setpnp(8)
